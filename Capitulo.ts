export class Capitulo {
  public _title: string;
  public _text: string;
  constructor(title: string, text: string,) {
    this._title = title;
    this._text = text;
  }

  public set title(title: string) {
    this._title = title;
  }

  public get title() {
    return this._title;
  }

  public setChapterinfo(title: string, text: string) {
    this._title = title;
    this._text = text;
  }
}