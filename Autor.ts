export class Autor {
  public _author: string;
  public _date: Date | undefined;
  constructor(author: string, date: Date) {
    this._author = author;
  }

  public getAuthorinfo(author: string, date: Date) {
    this._author = author;
    this._date = date;
  }
}