import { Autor } from "./Autor";
import { Capitulo } from "./Capitulo";

export class Livro {
  constructor(
    private _titlebook: string,
    private _code: string,
    private _author: Autor[],
    private _capitulo: Capitulo[],
  ) { }

  public set titlebook(titlebook: string) {
    this._titlebook = titlebook;
  }

  public set code(code: string) {
    this._code = code;
  }

  public get title() {
    return this._capitulo;
  }

  public get autors() {
    return this._author;
  }

  public adCapitulo(capitulo: Capitulo) {
    if (this._capitulo.length > 100) {
      return -1
    } else {
      this._capitulo.push(capitulo);
      return this._capitulo.length;
    }
  }

  public removeCapitulo(idcap: number) {
    if (this._capitulo[idcap]) {
      this._capitulo.splice(idcap, 1);
    } else {
      return -1
    }
  }
  public removeAutor(idauth: number) {
    if (this._author[idauth]) {
      this._author.splice(idauth, 1);
    } else {
      return -1
    }
  }

  public adAutor(autor: Autor) {
    if (this._author.length > 6) {
      return -1;
    } else {
      this._author.push(autor);
      return this._author.length;
    }
  }
}