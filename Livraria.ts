import prompt from "prompt-sync";
import { Capitulo } from "./Capitulo";
import { Autor } from "./Autor";
import { Livro } from "./Livro";

let keyboard = prompt();
let newLivro: Livro = new Livro('', '', [], []);

export class Livraria {
  constructor(
    private _livraria: Livro[],
  ) {
  }

  private cadastrarLivro = async () => {

    if (this._livraria.length <= 500) {

      let titlebook = keyboard('Digite o título do livro: ');
      let code = keyboard('Digite o ISBN: ');

      newLivro.titlebook = titlebook;
      newLivro.code = code;

      console.log('+------------------------------------------------+');

      this.cadastrarAutores();

      console.log('+------------------------------------------------+');

      this.cadastrarCap();

      this._livraria.push(newLivro);

      return console.log(`
        Livro cadastrado com sucesso na posição ${this._livraria.length}
      `);
    } else {
      return -1
    }
  }
  private cadastrarAutores() {

    let numAutors = +keyboard('Digite o número de autores a cadastrar: ');

    for (let i = 1; i <= numAutors; i++) {
      console.log(`Cadastro de Autor ( ${i} / ${numAutors} )`);

      let name = keyboard('Digite o nome do Autor: ');
      let date = new Date(keyboard('Digite a data de nascimento do Autor: '));

      let autor: Autor = new Autor(name, date);

      newLivro.adAutor(autor);
    }
  }

  private cadastrarCap() {

    let numCaps = +keyboard('Digite o número de capítulos a cadastrar: ');

    for (let i = 1; i <= numCaps; i++) {
      console.log(`Cadastro de Capítulo ( ${i} / ${numCaps} )`);

      let title = keyboard('Digite o título do capítulo: ');
      let text = keyboard('Digite o texto do capítulo: ');

      let cap: Capitulo = new Capitulo(title, text);

      newLivro.adCapitulo(cap);
    }
  }

  private listarAcervo() {
    if (this._livraria.length <= 0) {
      return console.log('Não há livros cadastrados.')
    } else {
      let livraria = [];
      for (let i = 0; i <= this._livraria.length; i++) {
        livraria.push({
          id: i,
          livro: this._livraria[i],
        })
      }
      return livraria;
    }
  }

  private modificarLivro() {

    console.log();
    console.log('+------------------------------------------------+');

    console.log(this._livraria);

    console.log('+------------------------------------------------+');
    console.log();

    let id = +keyboard('Digite o id do livro: ');

    console.log();
    console.log('+------------------------------------------------+');
    console.log('Oções de modificação de livro: ');

    if (this._livraria[id].title.length >= 100 && this._livraria[id].autors.length >= 6) {

      console.log();
      console.log('1 - Não é possível inserir mais autores neste livro.');
      console.log('2 - Não é possível inserir mais capítulos neste livro.');
      console.log();

    } else if (this._livraria[id].title.length < 100 && this._livraria[id].autors.length >= 6) {

      console.log();
      console.log('1 - Não é possível inserir mais autores neste livro.');
      console.log('2 - Para inserir capítulos.');
      console.log();

    } else if (this._livraria[id].title.length >= 100 && this._livraria[id].autors.length < 6) {

      console.log();
      console.log('1 - Para inserir autores.');
      console.log('2 - Não é possível inserir mais capítulos neste livro.');
      console.log();

    } else {
      console.log();
      console.log('1 - Para inserir autores.');
      console.log('2 - Para inseir capítulos.');
      console.log();
    }


    let op = +keyboard('Escolha uma opção: ');
    console.log();

    if (op == 1) {
      this.adCapLivro(id);
    } else if (op == 2) {
      this.adAutLivro(id);
    } else {
      return
    }
  }

  private adCapLivro(id: number) {
    let livro = this._livraria[id];

    console.log();

    let title = keyboard('Digite o título do capítulo: ');
    let text = keyboard('Digite o texto do capítulo: ');

    console.log();

    let cap: Capitulo = new Capitulo(title, text);

    livro.adCapitulo(cap);
  }

  private adAutLivro(id: number) {
    let livro = this._livraria[id];

    console.log();
    let name = keyboard('Digite o nome do autor: ');
    let date = new Date(keyboard('Digite a data de nascimento do autor: '));
    console.log();

    let autor: Autor = new Autor(name, date);

    livro.adAutor(autor);
  }

  private removeLivro() {
    if (this._livraria.length <= 0) {
      return console.log('Não há livros cadastrados.')
    } else {
      console.log();
      console.log('+------------------------------------------------+');
      console.log(this._livraria);
      console.log();
      console.log('+------------------------------------------------+');
      console.log();

      let id = +keyboard('Digite o id do livro que deseja remover: ')

      console.log(`O livro ${this._livraria[id]} foi removido com sucesso.`);
      this._livraria.splice(id, 1);

      console.log();
      console.log('+------------------------------------------------+');
    }

  }

  private listarCap() {
    console.log();
    console.log('+------------------------------------------------+');
    console.log(this._livraria);
    console.log();
    console.log('+------------------------------------------------+');

    let id = +keyboard('Digite o id do livro que deseja ver os capítulos: ')

    for (let i = 0; i <= this._livraria[id].title.length; i++) {
      let titulo = this._livraria[id].title[i]

      console.log();
      console.log(`Capítulo ${i + 1} - ${titulo}`);
    }
  }

  private resetLivrary() {
    this._livraria.splice(0, this._livraria.length);
  }

  private menu() {
    let option;
    let keyboard = prompt();
    do {
      console.log('+============= Livraria ============+')
      console.log('|1. Cadastrar novo livro               |')
      console.log('|2. Remover livro do acervo            |')
      console.log('|3. Listar acervo                      |')
      console.log('|4. Resetar livraria                   |')
      console.log('|5. Modificar livro do acervo          |')
      console.log('|6. Listar capítulos de livro       |')
      console.log('|9. Sair                               |')
      console.log('+======================================+')
      try {
        option = +keyboard('Escolha uma ação: ');
        switch (option) {
          case 1:
            this.cadastrarLivro();
          case 2:
            this.removeLivro();
          case 3:
            this.listarAcervo();
          case 4:
            this.resetLivrary();
          case 5:
            this.modificarLivro();
          case 6:
            this.listarCap();
          default:
            console.log('Opção inválida!');
        }
      } catch (_e) {
        throw new Error(_e);
      };
    } while (option != 9) {
      return
    }

    // console.log('+============= Livraria ============+')
    // console.log('|1. Cadastrar novo livro               |')
    // console.log('|2. Remover livro do acervo            |')
    // console.log('|3. Listar acervo                      |')
    // console.log('|4. Resetar livraria                   |')
    // console.log('|5. Modificar livro do acervo          |')
    // console.log('|6. Listar capítulos de livro       |')
    // console.log('|9. Sair                               |')
    // console.log('+======================================+')

    // option = +keyboard('Escolha uma ação: ');
    // switch (option) {
    //   case 1:
    //     this.cadastrarLivro();
    //   case 2:
    //     this.removeLivro();
    //   case 3:
    //     this.listarAcervo();
    //   case 4:
    //     this.resetLivrary();
    //   case 5:
    //     this.modificarLivro();
    //   case 6:
    //     this.listarCap();
    //   default:
    //     break;
    // }

  }
}
